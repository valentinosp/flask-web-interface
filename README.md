This is a basic web interface build in Flask. 
The purpose of this interface is provide the ability to the user to login on a device and control its functionality from the web interface. 
A simple example is to apply this interface on a raspberry pi and use it as a SmartHome Gateway in order to Monitor and Control several functionalities.

For the purpose which it will be used, it is build and configured in such a way that only the administrator can register/add other users and provide them with permitions to several functionalities.  

Documentation Work In Progress...