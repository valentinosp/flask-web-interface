
from flask import Flask, render_template

from flask_application import app

@app.errorhandler(404)
def not_found_error(error):
    return render_template('errortemplates/error404.html'), 404

@app.errorhandler(500)
def internal_error(error):
#    db.session.rollback()
    return render_template('errortemplates/error500.html'), 500

