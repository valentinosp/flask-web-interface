# for the flask login interface 
from flask import Flask, render_template
from flask import  flash, request, url_for, redirect, session
from flask import jsonify

from wtforms import Form, BooleanField, TextField, PasswordField, validators
from passlib.hash import sha256_crypt

from pymysql import escape_string as thwart

from functools import wraps

import gc
import re

from flask_application import app

from .databaseconnect import dbconnetion

import flask_application.errorhandling

## REQUIRED ########################################

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash("You are not Logged in, Login First!",'msg_warning')
            return redirect(url_for('login'))

    return wrap


def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'is_admin' in session:
            return f(*args, **kwargs)
        else:
            flash("You are not an Administrator, Access to admin area is denied",'msg_warning')
            return redirect(url_for('index'))

    return wrap

## FORMS #####################################

class RegistrationForm(Form):
    username = TextField('Username', [validators.Length(min=4, max=20)])
    email = TextField('Email Address', [validators.Length(min=6, max=50)])
    password = PasswordField('New Password', [
        validators.Required(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')

class ChangePasswordForm(Form):
    old_password = PasswordField('Old Password')
    password = PasswordField('New Password', [
    validators.Required(),
    validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')

##########################################

@app.route("/login/",methods=["GET","POST"])
def login():
    
    try:

        db_conn, cursor = dbconnetion()

        if request.method == "POST":

            if request.form['username'] != '':

                data_num = cursor.execute("Select uid, username , email, password , IsAdmin from users WHERE username = (%s)", thwart(request.form['username'])) 
            
                data = cursor.fetchall()

                data_row = data[0]

                password = data_row[3]

                IsAdmin = data_row[4]

                if sha256_crypt.verify(request.form['password'], password):

                    session['logged_in'] = True
                    session['username'] = request.form['username']

                    if IsAdmin == 1: 

                        session['is_admin'] = True

                    flash("Welcome, you have successfully login",'msg_success')

                    return redirect(url_for('index'))

            ## Invalid credentials

            flash("Invalid credentials, try again.", 'msg_error')

            return render_template('login.html')

        ## method = get

        gc.collect()

        return render_template('login.html')

    except Exception as e:
        #return(str(e))
        error = "Something went wrong when attempting Login, try again."
        return render_template('errortemplates/error500.html',error=error), 500

##########################################

@app.route("/logout/", methods=['GET'] )
@login_required
def logout():
    session.clear()
    flash("You have Logout!","msg_note")
    gc.collect()
    return redirect(url_for('login'))

##########################################

@app.route("/admin/", methods=['GET'] )
@login_required
@admin_required
def admin():

    return render_template('admin.html')

##########################################

@app.route("/admin/add_user/", methods=['GET','POST'] )
@login_required
@admin_required
def add_user():

    error = None 
    
    try:
        
        form = RegistrationForm(request.form)

        if request.method == "POST" and form.validate():
            
            if form.username.data != '' and form.email.data != '' and  form.password.data != '' :

                t_username  = thwart(form.username.data)
                t_email     = thwart(form.email.data)
                
                t_password  = thwart(form.password.data)
                
                hashed_password = sha256_crypt.encrypt(str(t_password)) 
                
                db_conn, cursor = dbconnetion()

                x = cursor.execute("SELECT * FROM users WHERE username = (%s)",(t_username))

                if int(x) > 0:

                    flash("The username already exists, please choose another",'msg_error')
                    
                    return render_template("add_user.html", form=form) 
                
                else:

                    cursor.execute("INSERT INTO users (username, password, email, IsAdmin ) VALUES (%s, %s, %s, 0 )",
                                (t_username, thwart(hashed_password), t_email ))
                    
                    db_conn.commit()
                    cursor.close()
                    db_conn.close()
                    gc.collect()

                    flash("User Created Succesfully",'msg_success')

                    return redirect(url_for('admin'))

            else: 

                flash("Invalid Input",'msg_error')
                    
                return render_template("add_user.html", form=form)

        else:

            return render_template("add_user.html", form=form) 

    except Exception as e:
        error = "Something went wrong when attempting to add user."
        return render_template('errortemplates/error500.html',error=error), 500

##########################################

@app.route("/user/", methods=['GET'] )
@login_required
def user():

    return render_template('user.html')


##########################################

@app.route("/user/changepassword/", methods=['GET','POST'] )
@login_required
def changepassword():

    try:
        
        form = ChangePasswordForm(request.form)

        if request.method == "POST" and form.validate():
            
            t_username  = thwart(session['username'])

            db_conn, cursor = dbconnetion()
            
            data_num = cursor.execute("Select uid, username , email, password , IsAdmin from users WHERE username = (%s)", t_username )
        
            data = cursor.fetchall()

            data_row = data[0]

            password = data_row[3]

            IsAdmin = data_row[4]

            if sha256_crypt.verify(request.form['old_password'], password):

                t_password  = thwart(form.password.data)
            
                hashed_password = sha256_crypt.encrypt(str(t_password)) 

                cursor.execute("Update users SET password = %s WHERE username=%s",
                        ( thwart(hashed_password), t_username  ) )

                db_conn.commit()
                cursor.close()
                db_conn.close()
                gc.collect()

                flash("Password Changed Succesfully",'msg_success')

                return redirect(url_for('user'))

            else: 

                flash("Invalid credentials, try again.", 'msg_error')
                
                return render_template("user_changepassword.html", form=form)
        
        return render_template("user_changepassword.html", form=form) 

    except Exception as e:
        
        error = "Something went wrong when attempting to change password."
        return render_template('errortemplates/error500.html',error=error), 500
