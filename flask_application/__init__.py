
from flask import  Flask, render_template, flash, request, url_for, redirect, session, jsonify
from wtforms import Form, BooleanField, TextField, PasswordField, validators
from passlib.hash import sha256_crypt
from pymysql import escape_string as thwart
from functools import wraps

import gc
import re

app = Flask(__name__)

app.secret_key = b'AelLaosProtathlima'

import flask_application.errorhandling
import flask_application.authenticate

from flask_application.databaseconnect import dbconnetion
from flask_application.authenticate    import login_required, admin_required

sidebar_blueprint_routes = {}

app.config['sidebar_blueprint_routes'] = sidebar_blueprint_routes

def add_sidebar_links(bp_routes_dict):

    global sidebar_blueprint_routes
    
    sidebar_blueprint_routes.update( bp_routes_dict ) 


###############################################

@app.route("/")
@login_required
def index():

    return render_template('index.html')

###############################################

@app.route("/example-page/", methods=['GET'] )
@login_required
def example_page():

    return render_template('example_page.html')

