#!/usr/bin/env python3

import os

user = "flask"

# function to ask user for [Y/N]

def execute ( command ) :
    while 1:
        yn = input("Proceed with: "+command+" [Y/N] ?")
        if( ( yn is 'Y' ) or (yn is 'y') ): 
            os.system(command)
            break
        elif( ( yn is 'N' ) or (yn is 'n') ): 
            print("continue")
            break
        else :
            print("answer Y or N")


# The script assumes the installation and configuration of the mysql server and creation of "user" 

# Create Mysql Authentication DB commands

execute ("sudo mysql -p -u root -e \"CREATE DATABASE authentication_db;\"")

execute ("sudo mysql -p -u root -e \"GRANT ALL PRIVILEGES ON authentication_db.* TO '%s'@'localhost'; FLUSH PRIVILEGES;\"" % (user))

execute ("sudo mysql -p -u root -e \"CREATE TABLE authentication_db.users2 (uid INT(11) AUTO_INCREMENT PRIMARY KEY, username VARCHAR(30), email VARCHAR(50), password VARCHAR(128), IsAdmin tinyint   );\"")

execute ("sudo mysql -p -u root -e \"CREATE TABLE authentication_db.login_attempts (uid INT(11), time varchar(30) );\"")