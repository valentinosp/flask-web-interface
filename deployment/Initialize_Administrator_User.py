#!/usr/bin/env python3

from pymysql import escape_string as thwart
from passlib.hash import sha256_crypt

from databaseconnect import dbconnetion

username = 'username' # SET username
password = 'password' # SET password
email    = 'email'    # SET email

t_username  = thwart(username)
t_password  = thwart(password)
t_email     = thwart(email)

# hash the password

hashed_password = sha256_crypt.encrypt( t_password )

#

db_conn, cursor = dbconnetion()

x = cursor.execute("SELECT * FROM users WHERE username = (%s)", (thwart(username)))

if int(x) > 0:
    print("\nThe username is already taken, please choose another")

else:

    cursor.execute("INSERT INTO users (username, password, email, IsAdmin ) VALUES (%s, %s, %s, 1 )",
    (thwart(username), thwart(hashed_password), thwart(email) ))

    db_conn.commit()
    print("\nAdministrator User Added!")

cursor.close()
db_conn.close()