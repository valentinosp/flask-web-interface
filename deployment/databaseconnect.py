import pymysql

from config import HOST, USER, PASSWORD, DATABASE

def dbconnetion():

    # Open database connection
    db_conn = pymysql.connect(HOST,USER,PASSWORD,DATABASE)

    # prepare a cursor object using cursor() method
    db_cursor = db_conn.cursor()

    return db_conn, db_cursor
