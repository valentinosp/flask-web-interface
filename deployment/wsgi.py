###################################################################
# IMPORT FROM THE MAIN APP 
###################################################################

from flask_application import app

from flask_application import add_sidebar_links

###################################################################
# IMPORT FROM BLUEPRINTS
###################################################################

from blueprints.blueprint_example import blueprint_example

from blueprints.blueprint_example import sidebar_route_dict as example_sidebar_route_dict

add_sidebar_links(example_sidebar_route_dict) 

app.register_blueprint( blueprint_example , url_prefix='/bp-example/' )

###################################################################

if __name__ == "__main__":
    app.run()
