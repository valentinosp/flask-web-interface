#!/usr/bin/env python3

from pymysql import escape_string as thwart
from passlib.hash import sha256_crypt

from databaseconnect import dbconnetion

username = 'username' # SET username
password = 'password' # SET password

t_username  = thwart(username)
t_password  = thwart(password)


# hash the password

hashed_password = sha256_crypt.encrypt( t_password )

# 

db_conn, cursor = dbconnetion()

x = cursor.execute("SELECT * FROM users WHERE username = (%s)", (thwart(username)))

if int(x) == 0:
    print("\nThe username does not exist")

else:

    cursor.execute("Update users SET password = %s WHERE username=%s",
                ( thwart(hashed_password), thwart(username)  ) )

    db_conn.commit()
    print("\nPassword Changed!")

cursor.close()
db_conn.close()