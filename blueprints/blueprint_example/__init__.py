from flask import Flask, render_template
from flask import flash, request, url_for, redirect, session

from pymysql import escape_string as thwart
import gc, re 

from flask import jsonify
from flask import Blueprint

# from config file import main app package name

from config import flask_main_package

# from local packages

from .blueprint_databaseconnect import bp_dbconnetion

# import Login required 

temp = __import__( flask_main_package , globals(), locals(), ['login_required'], 0)
 
login_required = temp.login_required

# define the blueprint

blueprint_example = Blueprint('blueprint_example', __name__, template_folder='templates', static_folder='static' )

# define sidebar links dictionary 

sidebar_route_dict = { 'Blueprint Example': {  1:   {  'BP Index'   : 'blueprint_example.index' } , 
                                               2:   {  'BP Example' : 'blueprint_example.example_page' } 
                                            }
                     }

###############################################

@blueprint_example.route("/")
@login_required
def index():

    return render_template('/blueprint_example/bp_index.html')

###############################################

@blueprint_example.route("/example-page/", methods=['GET'] )
@login_required
def example_page():

    return render_template('/blueprint_example/bp_example.html')
