import pymysql
 
from .config import HOST, USER, PASSWORD, DATABASE

def bp_dbconnetion():

    # Open database connection
    db_conn = pymysql.connect(HOST,USER,PASSWORD,DATABASE)

    db_cursor = db_conn.cursor()

    return db_conn, db_cursor